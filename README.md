# Omnilatam

This project exposes several services through an api as manage users, products, orders,
payments, and shipments, the project is developed in django rest framework

## Main dependencies
* Python 3.6
* Rabbitmq
* Celery 5.1.2
* Django 3.2.6
* djangorestframework 3.12

## Features

***
* Get access token Service http://127.0.0.1:8000/api/v1/token
* Product Service https://127.0.0.1:8000/api/v1/product/{action}
* Purchase Order Service https://127.0.0.1:8000/api/v1/order/{action}
* Payment https://127.0.0.1:8000/api/v1/payment/{action}
* Shipment Service https://127.0.0.1:8000/api/v1/shipment/{action}

## Installation

Docker

* You machine should has docker and docker-compose installed

```bash
$ docker-composer -f local.yml up
```

## Note

* Remember that to activate the services it is necessary to be authenticated, to authenticate and obtain the token it is necessary to point to the following url "http://127.0.0.1:8000/api/v1/token"  send the username and password