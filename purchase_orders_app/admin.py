# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from purchase_orders_app.models import Product, PurchaseOrder, Paid, ProductInPurchaseOrder


class CustomProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'value')


class CustomPurchaseOrderAdmin(admin.ModelAdmin):

    list_display = ('id', 'date', 'user', 'total')

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = ('total', )
        form = super(CustomPurchaseOrderAdmin, self).get_form(request, obj, **kwargs)
        return form


admin.site.register(Product, CustomProductAdmin)
admin.site.register(PurchaseOrder, CustomPurchaseOrderAdmin)
admin.site.register(ProductInPurchaseOrder)
admin.site.register(Paid)
