# Generated by Django 3.2.6 on 2021-08-16 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('purchase_orders_app', '0011_shipment_address'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipment',
            name='address',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
