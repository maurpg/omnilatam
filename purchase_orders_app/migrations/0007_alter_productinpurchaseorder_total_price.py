# Generated by Django 3.2.6 on 2021-08-15 17:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('purchase_orders_app', '0006_auto_20210815_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productinpurchaseorder',
            name='total_price',
            field=models.FloatField(blank=True),
        ),
    ]
