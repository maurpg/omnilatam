# Generated by Django 3.2.6 on 2021-08-16 15:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('purchase_orders_app', '0008_auto_20210816_0443'),
    ]

    operations = [
        migrations.CreateModel(
            name='Shipment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='purchase_orders_app.purchaseorder')),
                ('products', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='purchase_orders_app.productinpurchaseorder')),
            ],
        ),
    ]
