# Generated by Django 3.2.6 on 2021-08-13 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('purchase_orders_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseorder',
            name='total',
            field=models.PositiveIntegerField(default=None),
            preserve_default=False,
        ),
    ]
