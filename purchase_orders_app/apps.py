# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class PurchaseOrdersAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'purchase_orders_app'

    def ready(self):
        import purchase_orders_app.signals



