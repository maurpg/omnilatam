from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from purchase_orders_app.models import PurchaseOrder, Product


# @receiver(post_save, sender=PurchaseOrder)
# def save_total(sender, instance, **kwargs):
#     if instance.total is None:
#         instance.total = sum([product.value for product in instance.products.all()])
#         instance.save()
#         return instance
