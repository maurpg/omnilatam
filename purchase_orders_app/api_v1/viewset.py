from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from purchase_orders_app.api_v1.paginations import APIPagination
from purchase_orders_app.api_v1.serializers import ProductSerializer, PurchaseOrderSerializer, \
    PurchaseOrderCreateSerializer, PaidSerializer, ShipmentSerializer
from purchase_orders_app.models import Product, PurchaseOrder, Paid, Shipment


class ProductService(viewsets.ModelViewSet):
    """
    Class for all Api action of product Model , list , retrieve , update, create and delete Product,
    """
    # set queryset and serializer class for ProductService
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = APIPagination
    #permission_classes = (IsAuthenticated, )

    def retrieve(self, request, *args, **kwargs):
        product = get_object_or_404(self.queryset, pk=kwargs.get('pk'))
        serializer = self.get_serializer(product)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class PurchaseOrderService(viewsets.ModelViewSet):
    """
    Class for listing and retrieve purchase order, in this class
    only apply this two method
    """
    # set queryset and serializer class for PurchaseOrderService
    queryset = PurchaseOrder.objects.all()
    serializer_class = PurchaseOrderSerializer
    #permission_classes = (IsAuthenticated,)
    pagination_class = APIPagination

    def retrieve(self, request, *args, **kwargs):
        product = get_object_or_404(self.queryset, pk=kwargs.get('pk'))
        serializer = self.get_serializer(product)
        return Response(serializer.data)


class CreatePurchaseOrderService(CreateAPIView, UpdateAPIView):
    """
    Viewset Class for create and update purchase order
    """
    serializer_class = PurchaseOrderCreateSerializer
    queryset = PurchaseOrder
    #permission_classes = (IsAuthenticated,)
    pagination_class = APIPagination

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class PaymentService(viewsets.ModelViewSet):
    """
    Class for payment service
    """
    queryset = Paid.objects.all()
    serializer_class = PaidSerializer
    #permission_classes = (IsAuthenticated,)
    pagination_class = APIPagination


class ShipmentService(viewsets.ModelViewSet):
    """
    Class for Shipment Service
    """
    queryset = Shipment.objects.all()
    serializer_class = ShipmentSerializer
    #permission_classes = (IsAuthenticated,)
    pagination_class = APIPagination
