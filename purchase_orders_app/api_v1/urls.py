from django.urls import path
from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from purchase_orders_app.api_v1.viewset import ProductService, PurchaseOrderService, CreatePurchaseOrderService, \
    PaymentService, ShipmentService

urlpatterns = [
    #urls for login with jwt
    path('token', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),

    # url group for product
    path('product/list', ProductService.as_view({'get': 'list'}), name='product_list'),
    path('product/list/<int:pk>', ProductService.as_view({'get': 'retrieve'}), name='product_detail'),
    path('product/update/<int:pk>', ProductService.as_view({'put': 'update'}), name='product_detail'),
    path('product/delete/<int:pk>', ProductService.as_view({'delete': 'destroy'}), name='delete_product'),
    path('product/create', ProductService.as_view({'post': 'create'}), name='create_product'),


    path('order/list', PurchaseOrderService.as_view({'get': 'list'}), name='purchase_list'),
    path('order/list/<int:pk>', PurchaseOrderService.as_view({'get': 'retrieve'}), name='purchase_detail'),
    path('order/update<int:pk>', CreatePurchaseOrderService.as_view(), name='update_purchase_order'),
    path('order/delete<int:pk>', PurchaseOrderService.as_view({'delete': 'destroy'}), name='delete_purchase_order'),
    path('order/create', CreatePurchaseOrderService.as_view(), name='create_order'),

    path('paid/list', PaymentService.as_view({'get': 'list'}), name='paid_list'),
    path('paid/list/<int:pk>', PaymentService.as_view({'get': 'retrieve'}), name='paid_detail'),
    path('paid/update/<int:pk>', PaymentService.as_view({'put': 'update'}), name='update_paid'),
    path('paid/delete/<int:pk>', PaymentService.as_view({'delete': 'destroy'}), name='delete_paid'),
    path('paid/create', PaymentService.as_view({'post': 'create'}), name='create_paid'),

    path('shipment/list', ShipmentService.as_view({'get': 'list'}), name='shipment_list'),
    path('shipment/list/<int:pk>', ShipmentService.as_view({'get': 'retrieve'}), name='shipment_detail'),
    path('shipment/update/<int:pk>', ShipmentService.as_view({'put': 'update'}), name='update_shipment'),
    path('shipment/delete/<int:pk>', ShipmentService.as_view({'delete': 'destroy'}), name='delete_shipment'),
    path('shipment/create', ShipmentService.as_view({'post': 'create'}), name='create_shipment'),


    ]
