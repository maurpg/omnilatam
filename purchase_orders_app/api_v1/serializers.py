from django.contrib.auth.models import User
from django.db.models import Sum
from rest_framework import serializers
from rest_framework.response import Response
from purchase_orders_app.choices import PAID, PARTIAL_PAID, NOT_PAID
from purchase_orders_app.models import Product, PurchaseOrder, ProductInPurchaseOrder, Paid, Shipment
from purchase_orders_app.tasks import send_email


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'value')


class ProductInPurchaseOrderSerializer(serializers.ModelSerializer):
    product = serializers.CharField(source="product.name", read_only=True)

    class Meta:
        model = ProductInPurchaseOrder
        fields = ('product', 'quantity', 'total_price')


class PurchaseOrderSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.get_full_name", read_only=True)

    class Meta:
        model = PurchaseOrder
        fields = ('id', 'products', 'total', 'status', 'user')
        depth = 1


class PurchaseOrderCreateSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    products = serializers.JSONField()
    user = serializers.IntegerField()

    @staticmethod
    def get_user(user_id):
        """
        Function for search user in Model User, return instance if user exist
        else return raise exception
        :param user_id:
        :return:
        """
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise serializers.ValidationError('User not exist')

    @staticmethod
    def get_product(product):
        """
        Function for get product , receive id_product parameter and
        find in Product Model, if exist return object else return
        raiser exception
        :param product:
        :return: product object if product exist else exception
        """
        try:
            product_find = Product.objects.get(id=product.get('id_product'))
            return {'product': product_find, 'quantity': product.get('quantity')}
        except Product.DoesNotExist:
            raise serializers.ValidationError('Product with id {} not exist'.format(product.get('id_product')))

    def to_representation(self, instance):
        """
        method to_representation is overwritten to return the purchase order
        instance when it is created
        :param instance:
        :return: serializer.data
        """
        # serializer for list of purchase order
        serializer = PurchaseOrderSerializer(instance)
        return serializer.data

    def save_purchase_order_data(self, purchase_orders_object, list_valid_product):
        for product_for_create_in_order in list_valid_product:
            product_created_in_order = ProductInPurchaseOrder.objects.create(**product_for_create_in_order)
            purchase_orders_object.products.add(product_created_in_order)
        purchase_orders_object.total = sum([product.total_price for product in purchase_orders_object.products.all()])
        purchase_orders_object.save()
        return purchase_orders_object

    def create(self, validated_data):
        """
        :param validated_data:
        :return:
        Create method, the create method is overwritten to customize all
        the processes that are necessary before creating the purchase order,
        first validate that product exist after we created instance of ProductInPurchaseOrder
        and finally create purchase order
        """
        products = validated_data.get('products')
        user_id = validated_data.get('user', None)
        user = self.get_user(user_id)
        list_valid_product = [self.get_product(product) for product in products]
        # create purchase order
        purchase_orders_instance = PurchaseOrder.objects.create(user=user)
        purchase_orders_object = self.save_purchase_order_data(purchase_orders_instance, list_valid_product)
        return purchase_orders_object

    def update(self, instance, validated_data):
        """
        method to update a purchase order, only the product fields of the order and
        the user can be updated a list is received with the information of the products
        to update and the use
        :param instance:
        :param validated_data:
        :return:
        """
        if instance.status in [NOT_PAID]:
            products = validated_data.get('products', None)
            user_id = validated_data.get('user', instance.user)
            user = self.get_user(user_id)
            instance.user = user
            list_valid_product = [self.get_product(product) for product in products] if products is not None else None
            if list_valid_product:
                instance.products.clear()
                instance = self.save_purchase_order_data(instance, list_valid_product)
                return instance
            return instance
        raise serializers.ValidationError('order cannot be updated because its status is not unpaid')


class PaidSerializer(serializers.ModelSerializer):
    list_purchase = serializers.ListField(allow_null=True)

    class Meta:
        model = Paid
        fields = ('date', 'user', 'value', 'list_purchase')

    @staticmethod
    def get_purchase(purchase_id):
        """
        Function for get product , receive id_product parameter and
        find in Product Model, if exist return object else return
        raiser exception
        :param product:
        :return: product object if product exist else exception
        """
        try:
            return PurchaseOrder.objects.get(id=purchase_id)
        except PurchaseOrder.DoesNotExist:
            raise serializers.ValidationError('Purchase with id {} not exist'.format(purchase_id))

    def validate(self, validate_data):
        """
        Validate that paid not exceed value of purchase order
        :param validate_data:
        :return: validate date or raise exception
        """
        purchases_orders = [self.get_purchase(purchase_id) for purchase_id in validate_data.get('list_purchase')]
        for purchase in purchases_orders:
            value = validate_data.get('value')
            total = Paid.objects.filter(purchase=purchase).aggregate(Sum('value'))
            total = 0 if total.get('value__sum') is None else total.get('value__sum')
            if value + total > purchase.total:
                raise serializers.ValidationError('quantity exceeds the total value of the purchase order {}'.format(purchase.id))
            return validate_data

    def create(self, validated_data):
        """
        Create
        :param validated_data:
        :return:
        """
        list_payment = []
        for purchase in validated_data.get('list_purchase'):
            purchase = self.get_purchase(purchase)
            paid = Paid.objects.create(purchase=purchase, user=validated_data.get('user'),
                                       value=validated_data.get('value'))
            list_payment.append(paid)
        return list_payment[0]


class ShipmentSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="order.user.get_full_name", read_only=True)

    class Meta:
        model = Shipment
        fields = ('date', 'order', 'user', 'address')

    def create(self, validate_data):
        order = validate_data.get('order')
        address = validate_data.get('address')
        shipment = Shipment.objects.create(**validate_data)
        if shipment:
            send_email.delay(order.id, address)
        return shipment


