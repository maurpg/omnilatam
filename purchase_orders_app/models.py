# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Sum
from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
from purchase_orders_app.choices import PAID, PARTIAL_PAID, NOT_PAID


class Product(models.Model):
    name = models.CharField(max_length=100, null=True)
    description = models.CharField(max_length=100, null=True)
    value = models.FloatField()

    class Meta:
        unique_together = ('name', )

    def __str__(self):
        return self.name


class ProductInPurchaseOrder(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    total_price = models.FloatField(blank=True)

    def __str__(self):
        return f'name: {self.product.name} quantity :{self.quantity}'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.total_price = self.product.value * self.quantity
        super(ProductInPurchaseOrder, self).save(force_insert=False, force_update=False, using=None, update_fields=None)


class PurchaseOrder(models.Model):
    # List values of choice for field status
    status_order = (
        (PAID, 'Paid'),
        (PARTIAL_PAID, 'Partially Paid'),
        (NOT_PAID, 'Not Payed'),
    )
    products = models.ManyToManyField(ProductInPurchaseOrder, related_name='products_orders')
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name='user_purchase_order', on_delete=models.CASCADE)
    total = models.PositiveIntegerField(null=True, blank=True)
    status = models.CharField(max_length=50, choices=status_order, default=NOT_PAID)


class Shipment(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    order = models.ForeignKey(PurchaseOrder, on_delete=models.CASCADE)
    address = models.CharField(max_length=100, null=True)


class Paid(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name='user_paid', on_delete=models.CASCADE)
    value = models.PositiveIntegerField()
    purchase = models.ForeignKey(PurchaseOrder, related_name='purchase_paid', on_delete=models.CASCADE)

    class Meta:
        pass

    def get_all_paid_for_purchase(self):
        total = Paid.objects.filter(purchase=self.purchase).aggregate(Sum('value'))
        if total.get('value__sum') is None:
            total['value__sum'] = 0
        return total.get('value__sum')

    def clean(self):
        if self.purchase.status is Paid:
            raise ValidationError(_('purchase order is already paid'))

        if self.value + self.get_all_paid_for_purchase() > self.purchase.total:
            raise ValidationError(_('quantity exceeds the total value of the purchase order'))
