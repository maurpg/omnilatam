from django.urls import path, include

urlpatterns = [
    # Your URLs...
    path('', include('purchase_orders_app.api_v1.urls'), name='index'),

]
