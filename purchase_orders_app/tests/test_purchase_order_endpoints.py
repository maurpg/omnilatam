""" Module to define api purchase orders endpoints tests """
import json
from django.test import TestCase


class TestPurchaseOrderCreateEndpoint(TestCase):
    """ Test case for purchase order list endpoint /api/v1/list_orders/ """

    def test_list_purchase_orders(self):
        response = self.client.get('/api/v1/order/list')
        self.assertEqual(response.status_code, 200)

    def test_endpoint_create_purchase_order_with_product_non_existent(self):
        payload = {"products": [{
            "id_product": 1,
            "quantity": 2}],
            "user": 2}
        response = self.client.post('/api/v1/order/create', data=payload, content_type='application/json',)
        self.assertEqual(response.status_code, 400)

    def test_endpoint_create_purchase_order(self):
        payload = {"products": [{
            "id_product": 23,
            "quantity": 2}],
            "user": 2}
        response = self.client.post('/api/v1/order/create', data=payload, content_type='application/json',)
        self.assertEqual(response.status_code, 201)


