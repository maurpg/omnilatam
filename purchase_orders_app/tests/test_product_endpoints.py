""" Module to define api product endpoints tests """
import json
from django.test import TestCase


class TestProductCreateEndpoint(TestCase):
    """ Test case for product create endpoint /api/v1/products/action """

    def test_endpoint_should_avoid_create_products_with_the_same_name(self):
        payload = {
            'name': 'casa',
            'description': 'asdfa',
            'value': 300,
        }
        response = self.client.post('/api/v1/product/create', data=payload, content_type='application/json',)
        self.assertEqual(response.status_code, 201)

        response = self.client.post('/api/v1/product/create', data=payload, content_type='application/json',)
        self.assertEqual(response.status_code, 400)

    def test_endpoint_should_validate_mandatory_data(self):
        """ Endpoint /api/v1/products/ must validate the next data,
        {
            'name': 'string and mandatory',
            'description': 'string and mandatory',
            'value': 'string and mandatory'
        }
        if the data is malformed or missing a 400 response is expected.
        """

        bad_values = [
            {},
            {'name': 'mimosa'},
            {'name': 'dasfasf', 'description': 'aaa'},
            {'name': 'mimosa', 'description': 'aaaa', 'value': 'miasmdfs'},
        ]
        for payload in bad_values:
            response = self.client.post('/api/v1/product/create', data=payload, content_type='application/json', )
            self.assertEqual(response.status_code, 400)
