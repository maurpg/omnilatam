from django.conf.global_settings import EMAIL_HOST_USER
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from OmniLatam.celery import app
from purchase_orders_app.models import PurchaseOrder


@app.task
def send_email(order, address):
    """
    Function for send email with purchase order information to user, import EmailMessage from django.core.email
    param <User> user : user for send email
    """
    order = PurchaseOrder.objects.get(id=order)
    message = render_to_string("notification_purchase_order.html", {'order': order, 'address': address})
    mail = EmailMessage(
        subject="Purchase Order",
        body=message,
        from_email=EMAIL_HOST_USER,
        to=[order.user.email],
    )
    mail.content_subtype = "html"
    mail.send()
    return 'Notification was send'
