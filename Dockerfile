FROM python:3.6-slim

WORKDIR /

RUN apt-get update -y

RUN apt-get install libpq-dev gcc -y

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

VOLUME /src

COPY entrypoint.sh entrypoint.sh

RUN chmod +x entrypoint.sh

ENTRYPOINT /entrypoint.sh