""" Module to define general api_v1 urls """

from django.urls import path
from django.urls import include

urlpatterns = [
    path('', include('purchase_orders_app.api_v1.urls')),
]
